name := """px3_swig"""

version := "1.0"

scalaVersion := "2.11.0"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.1.3" % "test"

unmanagedSourceDirectories in Compile += baseDirectory.value / "px3_java"

