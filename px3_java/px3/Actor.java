/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package px3;

public class Actor {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected Actor(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Actor obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        PhysXJNI.delete_Actor(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public int testFunction() {
    return PhysXJNI.Actor_testFunction(swigCPtr, this);
  }

  public Actor() {
    this(PhysXJNI.new_Actor(), true);
  }

}
