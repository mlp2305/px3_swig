#define NDEBUG;
#include "engine.h"
#include <iostream>


using namespace physx;
namespace enginephysx {


	int  Actor::testFunction() {
			return 2;
		}

	void World::initPhysX() {
			PxDefaultErrorCallback defaultErrorCallback;
			PxDefaultAllocator defaultAllocatorCallback;
			foundation = PxCreateFoundation(PX_PHYSICS_VERSION,defaultAllocatorCallback, defaultErrorCallback);
			physics = PxCreatePhysics(PX_PHYSICS_VERSION, *foundation,PxTolerancesScale());
			PxInitExtensions(*physics);
			PxSceneDesc sceneDesc(physics->getTolerancesScale());
			sceneDesc.gravity = PxVec3(0.0f, -9.8f, 0.0f);
			sceneDesc.cpuDispatcher = PxDefaultCpuDispatcherCreate(1);
			sceneDesc.filterShader  = PxDefaultSimulationFilterShader;
			scene = physics->createScene(sceneDesc);
			step(0.01);

	}

	void World::step(float time) {
			this->scene->simulate(time);
			this->scene->fetchResults(true);
			callback->worldLooped(this);
		}
};
int main(int argc, char** argv) {
	using namespace enginephysx;
	using namespace std;
	/*World * w = new World();
	w->initPhysX();
	for(int i=0;i<1000;i++) {
		w->step(0.01);
	}*/
	return 0;
}

